<?php 
    get_header(); 
    get_template_part( 'partials/header', 'common', get_the_archive_title());
?>
    <div class="outer-container archive">
        <div class="inner-container">
            <main id="main" class="page-content-full">
            <?php 
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();
                        get_template_part( 'partials/card',  'article-archive');
                    }
                    the_posts_pagination(array(
                        'mid_size'  => 2,
                        'prev_text' => __( '<button class="button-round small bc-dark-grey"><i class="fas fa-chevron-left tc-lightest-grey"></i></button>', 'textdomain' ),
                        'next_text' => __( '<button class="button-round small bc-dark-grey"><i class="fas fa-chevron-right tc-lightest-grey"></i></button>', 'textdomain' ),
                    ) );
                } else {
                        // get_template_part( 'content', 'none' );
                }
                ?>
            </main>
        </div>
        <?php // get_template_part( 'partials/section',  'donate'); ?>
    </div>
<?php get_footer(); ?>