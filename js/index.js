import "../sass/style.scss";

import main_menu from "./components/main_menu";
import squares from "./components/squares";
import scroll_button from "./components/scroll_button";
import slider from "./components/slider";
import flippy from "./components/flippy_cards";
import carousel from "./components/carousel";
import blocks from "./components/blocks";
import popup from "./components/popup";

document.addEventListener('DOMContentLoaded', () => {
    main_menu();
    squares();
    scroll_button();
    slider();
    flippy();
    carousel();
    blocks();
    popup();
})