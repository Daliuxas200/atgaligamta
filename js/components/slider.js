import { tns } from "tiny-slider/src/tiny-slider";

export default () => {
  let sliders = [...document.querySelectorAll(".slider")];
  for (let slider of sliders) {
    tns({
      autoWidth: true,
      loop: true,
      center: true,
      mouseDrag: true,
      container: ".slider",
      startIndex: 2,
      speed: 400,
      gutter: 20,
      items: 1,
      nav: false,
      edgePadding: 20,
      swipeAngle: 30,
      prevButton: "#section-slider-previous-button",
      nextButton: "#section-slider-next-button",
    });
  }
};
