export default ()=>{

    // Sub menu opening and functionality
    let sub_menus = [...document.querySelectorAll('#main-menu .sub-menu')];
    for ( let el of sub_menus){
        let timer = 50;
        let mouse_over = false;

        el.parentElement.addEventListener("mouseover", e=>{
            el.style.display = 'block'
            setTimeout(()=>el.style.opacity = '1',0);
            mouse_over = true;
        })

        el.parentElement.addEventListener("mouseleave", e=>{
            mouse_over = false;
            setTimeout(()=>{
                if(!mouse_over){
                    el.style.opacity = '0'
                    setTimeout(()=>{
                        if(!mouse_over){
                            el.style.display = 'none'
                        }
                    },150)
                }
            },timer);
        })
    }

    // Mobile button functionality
    let mobile_nav_button = document.getElementById('mobile-menu-button');
    let mobile_nav = document.getElementById('mobile-menu');

    if(mobile_nav_button){
        mobile_nav_button.addEventListener('click',e=>{
            e.preventDefault();
            mobile_nav_button.querySelector('.main-nav__button__middle-bar').classList.toggle('active');
            mobile_nav.classList.toggle('active');
        })
    }

    // Search button functionality
    let search_bar_button = document.getElementById('search-bar-button');
    let search_bar = document.getElementById('search-bar');

    if(search_bar_button){
        search_bar_button.addEventListener('click',e=>{
            e.preventDefault();
            search_bar.classList.toggle('active');
        })
    }

    // CSS on scroll
    let main_nav = document.querySelector('.main-nav');
    if(main_nav){
        document.addEventListener('scroll',e=>{
            e.preventDefault();
            if(window.scrollY > 100){
                main_nav.classList.add('scrolled')
            } else {
                main_nav.classList.remove('scrolled')
            }
        })
    }
}