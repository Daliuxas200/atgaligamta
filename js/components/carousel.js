export default ()=>{

    class Slider{
        constructor(carousel){
            this.active_slide = 1;
            this.total_slides;
            this.changing = true;
            this.carousel = carousel;
            this.dotsContainer = this.carousel.querySelector('.carousel-dots');
            this.wait = this.carousel.dataset.wait;
            this.timer ;
            this.dots = {};
            this.slides = {};

        }

        setupDots(){
            for( let i = 1; i<=this.total_slides; i++){
                let dot = document.createElement("div"); 
                dot.classList.add("carousel-dot");
                dot.dataset.dotId = i;
                this.dotsContainer.appendChild(dot); 

                dot.addEventListener('click', e => {
                    e.preventDefault();
                    let targetId = e.target.dataset.dotId
                    this.toggleSlide();
                    this.updateState(targetId);
                    this.toggleSlide();
                    clearInterval(this.timer);
                    this.timer = this.autoChange();
                })

                this.dots[`dot_${i}`] = dot;
            }
        }

        setupSlides(){
            let slidesArr = [...this.carousel.querySelectorAll('.carousel-slide')];
            let i = 0;
            for(let slide of slidesArr){
                slide.dataset.slideId = ++i;
                this.slides[`slide_${i}`] = slide;
            }
            this.total_slides = i;
        }

        toggleSlide(){
            this.slides[`slide_${this.active_slide}`].classList.toggle('active');
            this.dots[`dot_${this.active_slide}`].classList.toggle('active');
        }

        updateState(targetId){
            if(targetId){
                this.active_slide = targetId;
            } else if(this.active_slide < this.total_slides){
                this.active_slide++;
            } else {
                this.active_slide = 1;
            }
        }

        autoChange(){
            return setInterval(() => {
                this.toggleSlide();
                this.updateState();
                this.toggleSlide();
            }, this.wait);
        }


        setup(){
            this.setupSlides();
            this.setupDots();
            this.toggleSlide();
            this.timer = this.autoChange();
        }

    }

    let carousel = document.getElementById('section-carousel');
    if(carousel){
        new Slider(carousel).setup();
    }
}