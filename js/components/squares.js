export default ()=>{

    let squares = [...document.querySelectorAll('.square-container')];

    let ro = new ResizeObserver( entries =>{
        for (let entry of entries) {
            checkAndResize(entry.target);
        }
    });

    function checkAndResize(square){
        let side = square.offsetHeight > square.offsetWidth ? square.offsetWidth : square.offsetHeight;
        let item = square.querySelector('.square-item');
        item.style.height = `${side}px`;
        item.style.width = `${side}px`;
    }

    for(let square of squares){
        checkAndResize(square);
        ro.observe(square);
    }

}