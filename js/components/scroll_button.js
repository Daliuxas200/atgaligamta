export default ()=>{

    let down = document.getElementById('header-scroll-down-button');
    let up = document.getElementById('footer-scroll-up-button');
    down && down.addEventListener('click',e=>{
        e.preventDefault();
        let offsetTop = document.getElementById('header').offsetHeight;
        scroll({
            top: offsetTop,
            behavior: "smooth"
        });
    })
    
    up && up.addEventListener('click',e=>{
        e.preventDefault();
        scroll({
            top: 0,
            behavior: "smooth"
        });
    })

}