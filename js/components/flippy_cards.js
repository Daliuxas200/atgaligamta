export default ()=>{

    let flippy_cards = [...document.querySelectorAll('.flippy-card')];

    for(let card of flippy_cards){
        let xClick;
        let yClick;
        let buttonClick = false;

        card.addEventListener('mousedown',e=>{
            e.preventDefault();
            xClick = e.clientX;
            yClick = e.clientY;
            buttonClick = e.target.classList.contains("button-standard");
        })

        card.addEventListener('mouseup',e=>{
            e.preventDefault();
            let xRelease = e.clientX;
            let yRelease = e.clientY;

            let vector = Math.sqrt(Math.pow(xClick-xRelease,2) + Math.pow(yClick-yRelease,2))
            if(vector < 10 && !buttonClick){
                 card.classList.toggle('flipped');
            }
        })
    }

}