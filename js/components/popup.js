export default ()=>{
    let popup = document.getElementById("popup");
    if(popup){
        let openButton = document.getElementById('popup-open-button');
        let openButtonMobile = document.getElementById('popup-open-button-mobile');
        let closeButton = document.getElementById('popup-close-button');

        let popupState = false;
        
        openButton.addEventListener('click',e => testPopup(true) )
        openButtonMobile.addEventListener('click',e => testPopup(true) )
        closeButton.addEventListener('click',e => testPopup(false) )

        function testPopup(action){
            if(!popupState && action){
                togglePopup(action);
            } else if(popupState && !action){
                togglePopup(action);
            }
        }

        function togglePopup(action){

            let timer = 100;
            if(action && !popupState){
                popup.classList.toggle('active');
                setTimeout(()=>{
                    popup.style.opacity = '1';
                    popupState = action;
                },0);
            } else if(popupState && !action){
                popup.style.opacity = '0';
                popupState = action;
                setTimeout(()=>{
                    popup.classList.toggle('active');
                },timer);
            }
        }
    }
}