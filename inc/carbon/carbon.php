<?php

use Carbon_Fields\Container;
use Carbon_Fields\Widget;
use Carbon_Fields\Field;
use Carbon_Fields\Block;



// Setup custom fields for page layout and such
add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {

    // Basic static card with Round image, title and some text
    function bubble_card_field(){
        return array(
            Field::make( 'image', 'image' ),
            Field::make( 'text', 'title' ),
            Field::make( 'text', 'text' ),
        );
    }

    // Complex field for displaying Static Bubbles
    function bubble_card_field_complex(){
        return array(
            Field::make( 'complex', 'crb_bubbles' )
                ->set_min(1)
                ->set_max(3)
                ->set_layout('tabbed-horizontal')
                ->add_fields( 'bubble', bubble_card_field() )
        );
    }

    // Fancy flipping card
    function flipping_card_field(){
        return array(
            Field::make( 'image', 'image' )
                ->set_width(20),
            Field::make( 'text', 'title' )
                ->set_width(40),
            Field::make( 'text', 'subtitle' )
                ->set_width(40),
            Field::make( 'complex', 'features' )
                ->set_collapsed(true)
                ->add_fields( 'features', array(
                    Field::make( 'text', 'feature' )
                ))
                ->set_help_text( 'Maximum number of features that can fit into card is 5, however it may be less if features contain a lot of text' )
                ->set_min(1)
                ->set_max(5),
            Field::make( 'text', 'button_text' )
                ->set_default_value( 'Sužinok daugiau' )
                ->set_width(20),
            Field::make( 'association', 'button_link' )
                ->set_types( array(
                    array(
                        'type'      => 'post',
                        'post_type' => 'page',
                    ),
                    array(
                        'type' => 'post',
                        'post_type' => 'post',
                    ),
                ) )
                ->set_min(1)
                ->set_max(1)
                ->set_width(80),
            );
    }

    // Fancy static card version of the flipping card
    function static_card_field(){
        return array(
            Field::make( 'image', 'image' )
                ->set_width(20),
            Field::make( 'text', 'title' )
                ->set_width(80),
            Field::make( 'text', 'button_text' )
                ->set_default_value( 'ieškok čia' )
                ->set_width(20),
            Field::make( 'association', 'button_link' )
                ->set_types( array(
                    array(
                        'type'      => 'post',
                        'post_type' => 'page',
                    ),
                    array(
                        'type'  => 'post',
                        'post_type' => 'post',
                    ),
                ) )
                ->set_min(1)
                ->set_max(1)
                ->set_width(80),
                );
    } 
    
    // Complex field group for displaying Flipping and static cards in a section or slider
    function flipping_card_field_complex(){
        return array(
            Field::make( 'text', 'title' ),
            Field::make( 'complex', 'crb_slider_cards' )
                ->set_min(2)
                ->set_layout('tabbed-horizontal')
                ->add_fields( 'flipping_card', flipping_card_field())
                ->add_fields( 'static_card', static_card_field())
        );
    } 
    
    // Support card 1
    function support_card_1_field(){
        return array(
            Field::make( 'image', 'image' )
                ->set_width(20),
            Field::make( 'text', 'title' )
                ->set_width(80),
            Field::make( 'textarea', 'text' ),
            Field::make( 'text', 'button_text' )
                ->set_default_value( 'Sužinok daugiau' )
                ->set_width(20),
            Field::make( 'association', 'button_link' )
                ->set_types( array(
                    array(
                        'type'      => 'post',
                        'post_type' => 'page',
                    ),
                    array(
                        'type' => 'post',
                        'post_type' => 'post',
                    ),
                ) )
                ->set_min(1)
                ->set_max(1)
                ->set_width(80),
        );
    }

    // Support card 2
    function support_card_2_field(){
        return array(
            Field::make( 'image', 'image' )
                ->set_width(20),
            Field::make( 'text', 'title' )
                ->set_width(80),
            Field::make('complex', 'list_items')
                ->add_fields('item',array(
                    Field::make('text','item')
                ))
            
        );
    }

    // Complex field for displaying multiple SVG cards in a section
    function support_card_field_complex(){
        return array(
            Field::make( 'text', 'title' ),
            Field::make( 'complex', 'crb_support_cards', 'Support Cards' )
                ->set_min(3)
                ->set_max(3)
                ->set_layout('tabbed-horizontal')
                ->add_fields( 'card', support_card_1_field())
        );
    } 

    // Complex field for displaying multiple support cards of style 2#
    function support_card_2_field_complex(){
        return array(
            Field::make( 'text', 'title' ),
            Field::make( 'complex', 'crb_support_cards', 'Support Cards' )
                ->set_min(3)
                ->set_max(3)
                ->set_layout('tabbed-horizontal')
                ->add_fields( 'card', support_card_2_field()),
            Field::make('text','text')
        );
    } 

    // Carousel section fields
    function carousel_field_complex(){
        return array(
            Field::make( 'text', 'title' ),
            Field::make( 'complex', 'crb_carousel_slides' )
                ->set_min(1)
                ->set_max(10)
                ->set_layout('tabbed-horizontal')
                ->add_fields( 'slide', array(
                    Field::make( 'text', 'text' ),
                ) ),
            Field::make( 'association', 'button_link' )
                ->set_types( array(
                    array(
                        'type'      => 'post',
                        'post_type' => 'page',
                    ),
                    array(
                        'type'      => 'post',
                        'post_type' => 'post',
                    ),
                ) )
                ->set_min(1)
                ->set_max(1),
            Field::make( 'text', 'button_text' )
                ->set_default_value( 'Sužinok daugiau' ),
            Field::make( 'select', 'autoplay_timeout' )
                ->set_options( array(
                    '1000' => '1s',
                    '2000' => '2s',
                    '3000' => '3s',
                    '4000' => '4s',
                    '5000' => '5s',
                ) )
                ->set_default_value( '3s' ),
        );
    }

    // About section fields
    function about_field(){
        return  array(
            Field::make('image','image'),
            Field::make('text','title'),
            Field::make('text','text'),
        );
    }

    // News section fields
    function news_field(){
        return array(
            Field::make('text','title'),
            Field::make( 'checkbox', 'crb_custom_news_posts', 'Manually select Posts' )
                ->set_option_value( 'yes' )
                ->set_help_text( 'Lets you select posts to display in this section, if left unchecked it will display newest entries' ),
            Field::make( 'association', 'crb_news_posts' )
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_custom_news_posts',
                        'value' => true,
                    )
                ) )
                ->set_types( array(
                    array(
                        'type' => 'post',
                        'post_type' => 'post',
                    ),
                ) )
                ->set_min(4)
                ->set_max(4),
            Field::make( 'association', 'news_link' )
                ->set_types( array(
                    array(
                        'type' => 'term',
                        'taxonomy' => 'category',
                    ),
                ) )
                ->set_min(1)
                ->set_max(1)
        );
    }

    // Pay section fields
    function donate_field(){
        return array(
            Field::make( 'text', 'donate_title' )
                ->set_default_value('Paremk mus'),
            Field::make( 'text', 'donate_text' )
                ->set_default_value('Mums labai svarbi jūsų parama, paaukoti pinigai bus panaudoti svetainės administravimui, žmonių švietimui, gyvūnėlių priežiūrai reikalingiem kaštam padengti. Prisidėti galite jums patogiu metodu!'),
        );
    }

    // Contacts small section field
    function contacts_small_field(){
        return array(
            Field::make('text','title')
                ->set_default_value('Su mumis galite susisiekti rašydami:'),
        );
    }

    // Large blocks of images and some text, used for volunteering section
    function large_blocks_field_complex(){
        return array(
            Field::make('text','title'),
            Field::make('complex','blocks')
                ->add_fields('block',array(
                    Field::make( 'image', 'image' ),
                    Field::make('text','title'),
                    Field::make('text','text'),
                ))
                ->set_min(1),
        );
    }

    // Field that encompasses all selections for Sections
    function sections_field($name){
        return array(
            Field::make( 'complex', $name )
                ->set_collapsed(true)
                ->add_fields( 'static_bubbles', bubble_card_field_complex())
                ->add_fields( 'cards_slider', flipping_card_field_complex())
                ->add_fields( 'support_cards', support_card_field_complex() )
                ->add_fields( 'support_cards_2', support_card_2_field_complex() )
                ->add_fields( 'carousel_slides', carousel_field_complex() )
                ->add_fields( 'about', about_field())
                ->add_fields( 'news', news_field())
                ->add_fields( 'donate', donate_field())
                ->add_fields( 'contacts-small', contacts_small_field())
                ->add_fields( 'large_blocks', large_blocks_field_complex())
        );
    }

    // // Container for other options like 404
    Container::make( 'theme_options', __( 'Other Theme Options' ) )
        ->add_tab( __( 'Main Menu Extra Options' ), array(
            Field::make( 'checkbox', 'crb_menu_button_1', 'Display Payment-Popup button in Menu' )
                ->set_option_value( 'yes' ),
            Field::make('text','crb_menu_button_1_title')
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_menu_button_1',
                        'value' => true,
                    )
                ) ),
            Field::make( 'checkbox', 'crb_menu_button_2', 'Display Customiseable button in Menu' )
                ->set_option_value( 'yes' ),
            Field::make( 'association', 'crb_menu_button_2_link' )
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_menu_button_2',
                        'value' => true,
                    )
                ) )
                ->set_types( array(
                    array(
                        'type' => 'post',
                        'post_type' => 'page'
                    ),
                    array(
                        'type' => 'post',
                        'post_type' => 'post'
                    )
                ) )
                ->set_min(1)
                ->set_max(1),
            Field::make('text','crb_menu_button_2_title')
                ->set_conditional_logic( array(
                    array(
                        'field' => 'crb_menu_button_2',
                        'value' => true,
                    )
                ) )
        ) )  
        ->add_tab( __( 'Missing Page' ), array(
            Field::make( 'text', 'crb_fourofour_subtitle', 'Missing page subtitle'),
            Field::make( 'image', 'crb_fourofour_image', 'Missing page image')    
        ) )
        ->add_tab( __( 'Social Links' ), array(
            Field::make( 'complex', 'crb_social' )
                ->set_help_text( 'Select class for icon from Font Awesome website, all regular free icons should work' )
                ->set_layout('tabbed-vertical')
                ->add_fields( 'card', array(
                    Field::make('text','icon_class'),
                    Field::make('text','link'),
                )),
        ) )
        ->add_tab(__( 'Contact Buttons' ), array(
            Field::make('text','crb_contacts_header')->set_default_value('Su mumis galite susisiekti:'),
            Field::make('text','crb_messenger_title')->set_default_value('Facebook žinute'),
            Field::make('text','crb_messenger_link')->set_default_value('https://m.me/atgaligamta'),
            Field::make('text','crb_email_title')->set_default_value('info@atgaligamta.org'),
            Field::make('text','crb_email_link')->set_default_value('info@atgaligamta.org'),
        ))
        ->add_tab(__( 'Support settings' ), array(
            Field::make('text','crb_support_title')->set_default_value('Paremk mus'),
            Field::make('text','crb_support_text')->set_default_value('Mums labai svarbi jūsų parama, paaukoti pinigai bus panaudoti svetainės administravimui, žmonių švietimui, gyvūnėlių priežiūrai reikalingiem kaštam padengti. Prisidėti galite jums patogiu metodu!'),
            Field::make( 'complex', 'crb_support_options' )
                ->set_layout('tabbed-vertical')
                ->add_fields( 'support_option', array(
                    Field::make('image','image'),
                    Field::make('text','link'),
                )),
        ));
        
    // Container for Home Page customisation
    Container::make( 'theme_options', __( 'Home Page Options' ) )
        ->add_tab( __( 'Header' ), array(
            Field::make( 'text', 'crb_header_title'),
            Field::make( 'text', 'crb_header_subtitle'),
            Field::make( 'image', 'crb_header_image')    
        ) )
        ->add_tab( __( 'Sections' ), sections_field('crb_sections'))
        ->add_tab( __( 'Footer' ), array(
            Field::make( 'image', 'crb_footer_image'),
            Field::make( 'text', 'crb_footer_top_text_1')
                ->set_default_value('Su mumis galite susisiekti adresu'),
            Field::make( 'text', 'crb_footer_top_text_2')
                ->set_default_value('info@atgaligamta.org'),
            Field::make( 'text', 'crb_footer_bottom_text_1')
                ->set_default_value('VšĮ „Pagalba laukiniams gyvūnams“ '),
            Field::make( 'text', 'crb_footer_bottom_text_2')
                ->set_default_value('Įmonės kodas: 305068894 '),
        ) );
        

    // Block for Q/A type expander
    Block::make( __( 'Expander' ) )
        ->add_fields( array(
            Field::make( 'text', 'expander_heading', __( 'Expander Heading' ) ),
            Field::make( 'rich_text', 'expander_content', __( 'Expander Content' ) ),
        ) )
        ->set_description( __( 'A question/answer expandable block for your needs' ) )
        ->set_render_callback( function ( $fields, $attributes, $inner_blocks ) {
            ?>

            <details class="expander-block">
                <summary class="expander-block__heading">
                    <div class="expander-block__title"><?php echo esc_html( $fields['expander_heading'] ); ?></div>
                    <div class="expander-block__button button-round bc-dark-grey small">
                            <i class="fas fa-chevron-down tc-lightest-grey"></i>
                    </div>
                </summary>

                <div class="expander-block__content">
                    <div class="expander-block__content__inner">
                        <?php echo apply_filters( 'the_content', $fields['expander_content'] ); ?>
                    </div>
                </div>
            </details>

            <?php
        } );

    
    // Post meta for page sections
    Container::make( 'post_meta', __( 'Add more Sections' ) )
        ->where( 'post_type', '=', 'page' )
        ->add_fields(sections_field('crb_page_sections') );


    // setup custom widgets
    class SimpleInfoWidget extends Widget {
        // Register widget function. Must have the same name as the class
        function __construct() {
            $this->setup( 'simple_info_widget', 'Basic Info Widget', 'Displays a logo with some text and contact info', array(
                Field::make( 'image', 'logo', 'Logo' ) ,
                Field::make( 'rich_text', 'content', 'Content' ),
            ) );
            
        }
        
        // Called when rendering the widget in the front-end
        function front_end( $args, $instance ) { 
            $fb_text = carbon_get_theme_option('crb_messenger_title');
            $fb_link = carbon_get_theme_option('crb_messenger_link');
            $email_text = carbon_get_theme_option('crb_email_title');
            $email_link = carbon_get_theme_option('crb_email_link');
            $contacts_header = carbon_get_theme_option('crb_contacts_header');
            ?>
            <img class="widget__logo" src=' <?php echo wp_get_attachment_image_src($instance['logo'], $size = 'medium')[0]; ?>'>
            <?php echo apply_filters( 'the_content',$instance['content']); ?>
            <div class="widget__subtitle"><?php echo $contacts_header; ?></div>
            <a class="widget__button" target="_blank" href="<?php echo $fb_link;?>">  
                <?php echo $fb_text; ?>
            </a>
            <a class="widget__button" target="_blank" href="mailto:<?php echo $email_link;?>">  
                <?php echo $email_text; ?>
            </a>
            <?php
        }
    }

    // function load_widgets() {
    //     register_widget( 'SimpleInfoWidget' );
    // }
}

// add_action( 'widgets_init', 'load_widgets' );