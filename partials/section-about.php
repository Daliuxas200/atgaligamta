<!-- section about us or contact -->
<?php
    $fb_text = carbon_get_theme_option('crb_messenger_title');
    $fb_link = carbon_get_theme_option('crb_messenger_link');
    $email_text = carbon_get_theme_option('crb_email_title');
    $email_link = carbon_get_theme_option('crb_email_link');
    $contacts_header = carbon_get_theme_option('crb_contacts_header');
?>
<section class="section section-about" id="section-about">
    <div class="section-about__container">
        <div class="section-about__image-block" style="background-image: url('<?php echo wp_get_attachment_image_src($args['image'], $size = 'chonky-medium')[0]; ?>');"></div>
        <div class="section-about__text-block">
            <h3 class="section-about__title"><?php echo $args['title']; ?></h3>
            <p class="section-about__text"><?php echo $args['text']; ?></p>
            <h6 class="section-about__buttons-heading"><?php echo $contacts_header; ?></h6>
            <div class="section-about__buttons-container">
                <a class="section-about__button button-standard transparent small" href="<?php echo $fb_link; ?>"><?php echo $fb_text; ?></a>
                <a class="section-about__button button-standard transparent small" href="mailto:<?php echo $email_link; ?>"><?php echo $email_text; ?></a>
            </div>
        </div>
    </div>
</section>