<?php 
    $title = $args["title"];
    $read_more_link = get_category_link($args["link"][0]['id']);

    if($args["custom"]){
        $post_ids = [];
        foreach($args["posts"] as $post){
            array_push($post_ids, $post['id']);
        }
        $posts = get_posts([
            'include'=>$post_ids,
            'orderby'=>'post__in'
            ]);
    } else {
        $posts = get_posts([
            'numberposts'=>4
        ]);
    }
?>

 <!-- Section single article --> 
 <section id="section-news" class="section section-news">
    <div class="section-news__container">
        <h4 class="section-news__title"><?php echo $title ?></h4>

        <div class="section-news__main-container">
            <?php
                $main_image = get_the_post_thumbnail_url($posts[0]->ID,'chonky-medium');
                $main_title = $posts[0]->post_title;
                $main_excertp = get_the_excerpt($posts[0]->ID);
                $main_link = get_post_permalink($post->ID);
                $main_category = get_the_category($posts[0]->ID)[0];
                $main_category_link = get_term_link($main_category->term_id);

                get_template_part( 'partials/card',  'article-wide' , [
                    "title" => $main_title,
                    "image" => $main_image,
                    "excerpt" => $main_excertp,
                    "link" => $main_link,
                    "category" => $main_category->name,
                    "category_link" => $main_category_link
                ]);
            ?>
        </div>

        <div class="section-news__cards-container">
            <?php foreach( $posts as $key=>$post){ 
                $image = get_the_post_thumbnail_url($post->ID,'chonky-medium');
                $title = $post->post_title;
                $excerpt = get_the_excerpt($post->ID);
                $link = get_post_permalink($post->ID);
                $category = get_the_category($post->ID)[0];
                $category_link = get_term_link($category->term_id);

                get_template_part( 'partials/card',  'article' , [
                    "title" => $title,
                    "image" => $image,
                    "excerpt" => $excerpt,
                    "link" => $link,
                    "category" => $category->name,
                    "category_link" => $category_link
                ]);
            }; ?>
        </div>
        <a href="<?php echo $read_more_link?>" class="section-news__cta button-standard big transparent">Daugiau įrašų</a>
    </div>
</section>