<div class="flippy-card">
    <div class="flippy-card__front">
        <img class="flippy-card__image" src='<?php echo wp_get_attachment_image_src($args['image'], $size = 'medium')[0]; ?>'>
        <h5 class="flippy-card__title"><?php echo $args['title']; ?></h5>
        <h5 class="flippy-card__subtitle"><?php echo $args['subtitle']; ?></h5>
    </div>
    <div class="flippy-card__back">
        <h5 class="flippy-card__title"><?php echo $args['title']; ?></h3>
        <h5 class="flippy-card__subtitle"><?php echo $args['subtitle']; ?></h4>
        <ul class="flippy-card__list">
            <?php foreach($args['features'] as $feature): ?>
                <li class="flippy-card__list-item"><?php echo $feature['feature']; ?></li>
            <?php endforeach; ?>
        </ul>
        <a class="flippy-card__back__button button-standard transparent small" target="_blank" href='<?php echo get_permalink($args["button_link"][0]['id']); ?>'><?php echo $args['button_text']; ?></a>
    </div>
</div>
