<?php 
    $title = $args["title"];
    $cards = $args["cards"];
    $text = $args["text"];
    $fb_text = carbon_get_theme_option('crb_messenger_title');
    $fb_link = carbon_get_theme_option('crb_messenger_link');
    $email_text = carbon_get_theme_option('crb_email_title');
    $email_link = carbon_get_theme_option('crb_email_link');
?>

<section class="section section-support" id="section-support">
    <div class="section-support__container">
        <h3 class="section-support__title"><?php echo $title ?></h3>
        <div class="section-support__cards-container">
            <?php 
                foreach ( $cards as $card ) { 
                    get_template_part( 'partials/card', 'support-2', $card  );
                } 
            ?>
        </div>
        <div class="section-support__bottom">
            <div class="section-support__text">
                <?php echo $text; ?>
            </div>
            <div class="section-support__buttons">
                <a href="<?php echo $email_link;?>" class="section-support__button button-standard transparent small"><?php echo $email_text;?></a>
                <a href="<?php echo $fb_link;?>" class="section-support__button button-standard transparent small"><?php echo $fb_text;?></a>
            </div>
        </div>
    </div>
</section>

                        
