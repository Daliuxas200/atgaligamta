<div class="support-card">
    <div class="support-card__circle">
        <img class="support-card__circle__img" src=' <?php echo wp_get_attachment_image_src($args['image'], $size = 'medium')[0]; ?>'>
    </div>
    <h5 class="support-card__title-2"><?php echo $args['title'];?></h5>
    <ul class="support-card__list">
        <?php foreach($args['list_items'] as $item): ?>
            <li class="support-card__list-item"><?php echo $item['item']; ?></li>
        <?php endforeach; ?>
    </ul>
</div>