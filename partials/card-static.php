<div class="flippy-card">
    <div class="flippy-card__single">
            <img class="flippy-card__single__image" src='<?php echo wp_get_attachment_image_src($args['image'], $size = 'medium')[0]; ?>'>
        <h5 class="flippy-card__single__title"><?php echo $args['title']; ?></h5>
        <a class="flippy-card__single__button button-standard transparent-white small" href="<?php get_permalink($args["button_link"][0]['id']); ?>"><?php echo $args['button_text']; ?></a>
    </div>
</div>
