<!-- section carousel -->
<section class="section section-pay" id="section-pay">
    <div class="section-pay__container">
        <h2 class="section-pay__title"><?php echo $args['donate_title'];?></h2>    
        <div class="section-pay__text"><?php echo $args['donate_text'];?></div>
        <div class="section-pay__buttons">
            <?php $supportOptions = carbon_get_theme_option( 'crb_support_options' ); ?>
                <?php foreach($supportOptions as $option):?>
                    <a target="_blank" class="section-pay__button" href="<?php echo $option['link']; ?>"><img src="<?php echo wp_get_attachment_image_src($option['image'], $size = 'Medium')[0]; ?>" alt=""></a>
                <?php endforeach; ?>
            </div>
    </div>
</section>