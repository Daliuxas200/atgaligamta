<?php
$sections = $args;
foreach ( $sections as $section ) {
    switch ( $section['_type'] ) {
        case 'static_bubbles':
            get_template_part( 'partials/section', 'bubbles', $section['crb_bubbles']  );
            break;
        case 'cards_slider':
            get_template_part( 'partials/section', 'slides', ["title" => $section['title'],"cards" => $section['crb_slider_cards']]  );
            break;
        case 'support_cards':
            get_template_part( 'partials/section', 'support', ["title" => $section['title'],"cards" => $section['crb_support_cards']]  );
            break;
        case 'support_cards_2':
            get_template_part( 'partials/section', 'support-2', ["title" => $section['title'],"cards" => $section['crb_support_cards'],"text" => $section['text']]  );
            break;
        case 'carousel_slides':
            get_template_part( 'partials/section', 'carousel', $section );
            break;
        case 'about':
            get_template_part( 'partials/section', 'about', $section );
            break;
        case 'news':
            get_template_part( 'partials/section',  'news' , ["link" => $section['news_link'], "title" => $section['title'],"posts" => $section['crb_news_posts'],"custom" => $section['crb_custom_news_posts']]);
            break;
        case 'donate':
            get_template_part( 'partials/section',  'donate' , $section);
            break;
        case 'contacts-small':
            get_template_part( 'partials/section',  'contacts-small' , $section);
            break;
        case 'large_blocks':
            get_template_part( 'partials/section',  'large-blocks' , $section);
            break;
    }
}