<?php 
    $title = $args["title"];
    $cards = $args["cards"];
?>

<!-- Sliding cards section -->
<section id="section-slider" class="section section-slider ">

<div class="section-slider__top">
    <h2 class="section-slider__title"><?php echo $title; ?></h2>
    <div class="section-slider__controls">
        <button class="button-round small section-slider__controls__previous" id="section-slider-previous-button">
            <i class="fas fa-chevron-left"></i>
        </button>
        <button class="button-round small section-slider__controls__next" id="section-slider-next-button">
            <i class="fas fa-chevron-right"></i>
        </button>
    </div>  
</div>

<div class="section-slider__slider ">
    <div class="slider">
        <?php 
            foreach($cards as $card){
                echo '<div>';
                switch($card['_type']){
                    case 'flipping_card': 
                        get_template_part( 'partials/card', 'flipping', $card  );
                        break;
                    case 'static_card': 
                        get_template_part( 'partials/card', 'static', $card  );
                        break;
                }
                echo '</div>';
            }
        ?>

    </div>
</div>
</section>