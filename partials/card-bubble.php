<div class="section-bubbles__bubble">
    <img class="section-bubbles__bubble__picture" src=' <?php echo wp_get_attachment_image_src($args['image'], $size = 'medium')[0]; ?>'>
    <div class="section-bubbles__bubble__text-box">
        <h3 class="section-bubbles__bubble__title"><?php echo $args['title'];?></h3>    
        <p class="section-bubbles__bubble__text"><?php echo $args['text'];?></p>
    </div>
</div>