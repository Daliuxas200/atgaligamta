<article class="article-card">
    <a class="article-card__image-block" href="<?php echo $args['link'];?>" style="background-image: url('<?php echo $args['image']; ?>');"></a>
    <div class="article-card__text-block">
        <div class="breadcrumbs"><a href="<?php echo $args['category_link']; ?>" > <?php echo $args['category']; ?> </a></div>
        <h5 class="article-card__title"><a href="<?php echo $args['link'];?>"><?php echo $args['title'];?></a></h5>
        <div class="article-card__excerpt">
            <p><?php echo $args['excerpt']; ?></p>
        </div>  
        <a class="article-card__read-more-button button-underlined" href="<?php echo $args['link']; ?>">skaityti toliau</a>                    
    </div>   
</article>