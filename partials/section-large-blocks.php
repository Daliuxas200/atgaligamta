<section id="section-volunteering" class="section section-volunteering">
    <div class="section-volunteering__container">
        <h4 class="section-volunteering__title"><?php echo $args['title']; ?></h4>
        <?php foreach ( $args['blocks'] as $block ): ?>
        <div class="section-volunteering__block">
            <div class="section-volunteering__block__left" style="background-image: url('<?php echo wp_get_attachment_image_src($block['image'], $size = 'chonky-medium')[0]; ?>');"></div>
            <div class="section-volunteering__block__right">
                <h3 class="section-volunteering__block__title"><?php echo $block['title']; ?></h3>
                <p class="section-volunteering__block__text"><?php echo $block['text']; ?></p>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</section>