<?php 
    $title = $args["title"];
    $timeout = $args["autoplay_timeout"];
    $slides = $args["crb_carousel_slides"];
    $button_text = $args["button_text"];
    $button_link = get_permalink($args["button_link"][0]['id']);
?>

<!-- section carousel -->
<section class="section section-carousel carousel" id="section-carousel" data-wait='<?php echo $timeout; ?>'>
    <div class="section-carousel__container">
        <h5 class="section-carousel__supertitle"><?php echo $title; ?></h5>
        <div class="section-carousel__dots carousel-dots"></div>

        <div class="section-carousel__slides">
            <?php foreach($slides as $slide): ?>
                <h2 class="section-carousel__title carousel-slide" ><?php echo $slide['text']; ?></h2>
            <?php endforeach; ?>
        </div>

        <a class="section-carousel__button button-standard big transparent-white" href="<?php echo $button_link; ?>" ><?php echo $button_text; ?></a>
    </div>
</section>