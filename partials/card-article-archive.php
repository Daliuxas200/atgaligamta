
<?php 
    $ID = get_the_ID(); 
    $main_category = get_the_category($ID)[0];
    $main_category_link = get_term_link($main_category->term_id);
?>
<article class="article-card-archive">
    <a class="article-card-archive__image-block" href="<?php echo get_post_permalink() ;?>" style="background-image: url('<?php echo get_the_post_thumbnail_url( $ID, 'chonky-medium' ); ?>');"></a>
    <div class="article-card-archive__text-block">
        <?php if($main_category): ?>
            <div class="breadcrumbs"><a href="<?php echo $main_category_link; ?>" > <?php echo $main_category->name; ?> </a></div>
        <?php endif; ?>
        <h3 class="article-card-archive__title"><a href="<?php echo get_post_permalink() ;?>"><?php echo get_the_title();?></a></h3>
        <div class="article-card-archive__excerpt">
            <p><?php echo get_the_excerpt(); ?></p>
        </div>  
        <a class="article-card-archive__read-more-button button-underlined" href="<?php echo get_post_permalink(); ?>">skaityti toliau</a>                    
    </div>   
</article>