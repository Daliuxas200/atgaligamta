<section id="section-bubbles" class="section section-bubbles">
    <?php 
        foreach ( $args as $bubble ) { 
            get_template_part( 'partials/card', 'bubble', $bubble  );
        }
    ?>
</section>