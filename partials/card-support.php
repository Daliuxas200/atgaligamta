<div class="support-card">
    <div class="support-card__circle">
        <img class="support-card__circle__img" src=' <?php echo wp_get_attachment_image_src($args['image'], $size = 'medium')[0]; ?>'>
    </div>
    <h5 class="support-card__title"><?php echo $args['title'];?></h5>
    <p class="support-card__text"><?php echo $args['text'];?></p>
    <a href="<?php echo get_permalink($args["button_link"][0]['id']); ?>" class="support-card__button button-standard small transparent"><?php echo $args['button_text'];?></a>
</div>