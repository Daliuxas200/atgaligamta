<?php 
    $title = $args["title"];
    $fb_text = carbon_get_theme_option('crb_messenger_title');
    $fb_link = carbon_get_theme_option('crb_messenger_link');
    $email_text = carbon_get_theme_option('crb_email_title');
    $email_link = carbon_get_theme_option('crb_email_link');
?>

<div class="section-contacts-small">
    <div class="section-contacts-small__container">
        <h2 class="section-contacts-small__title"><?php echo $title; ?></h2>
        <div class="section-contacts-small__buttons">
            <a href="mailto:<?php echo $email_link;?>" class="section-contacts-small__button button-standard transparent-white small"><?php echo $email_text;?></a>
            <a href="<?php echo $fb_link;?>" class="section-contacts-small__button button-standard transparent-white small"><?php echo $fb_text;?></a>
        </div>
    </div>
</div>