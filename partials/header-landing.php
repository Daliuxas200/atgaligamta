<header id="header" class="landing-header">
    <div class="landing-header__container">
        <div class="landing-header__title">
            <?php
                $title = carbon_get_theme_option( 'crb_header_title' );
                if(!$title) $title = get_bloginfo('name');

                $subtitle = carbon_get_theme_option( 'crb_header_subtitle' );
                if(!$subtitle) $subtitle = get_bloginfo('description');

                $image = carbon_get_theme_option( 'crb_header_image' );
            ?>
            <h1 class="landing-header__title__main" id="main-header"><?php echo $title; ?></h1>
            <h2 class="landing-header__title__sub" id="sub-header"><?php echo $subtitle; ?></h2>
            <div class="landing-header__title__button">
                <button class=" button-round big bc-dark-grey tc-lightest-grey" id="header-scroll-down-button">
                    <i class="fas fa-chevron-down"></i>
                </button>
            </div>
        </div>
        <div class="landing-header__image-container square-container">
            <div class="landing-header__image square-item" style="background-image:url(' <?php echo wp_get_attachment_image_src($image, $size = 'chonky-medium')[0]; ?>')"></div>
        </div>
    </div>
</header>