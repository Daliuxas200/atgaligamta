<article class="article-card-wide">
    <a class="article-card-wide__image-block" href="<?php echo $args['link'];?>" style="background-image: url('<?php echo $args['image']; ?>');"></a>
    <div class="article-card-wide__text-block">
        <div class="breadcrumbs"><a href="<?php echo $args['category_link']; ?>" > <?php echo $args['category']; ?> </a></div>
        <h3 class="article-card-wide__title"><a href="<?php echo $args['link'];?>"><?php echo $args['title'];?></a></h3>
        <div class="article-card-wide__excerpt">
            <p><?php echo $args['excerpt']; ?></p>
        </div>  
        <a class="article-card-wide__read-more-button button-underlined" href="<?php echo $args['link']; ?>">skaityti toliau</a>                    
    </div>   
</article>