<?php 
    $title = $args["title"];
    $cards = $args["cards"];
?>

<section class="section section-support" id="section-support">
    <div class="section-support__container">
        <h3 class="section-support__title"><?php echo $title ?></h3>
        <div class="section-support__cards-container">
            <?php 
                foreach ( $cards as $card ) { 
                    get_template_part( 'partials/card', 'support', $card  );
                } 
            ?>
        </div>
    </div>
</section>

                        
