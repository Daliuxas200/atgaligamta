
    <footer class="footer-container" id="footer">
        <div class="footer-content">
            
            <div class="footer-content__left">
                <div class="footer-block">
                    <span class="footer-text"> <?php echo carbon_get_theme_option( 'crb_footer_top_text_1' ); ?></span>
                    <div class="break"></div>
                    <span class="footer-text"> <?php echo carbon_get_theme_option( 'crb_footer_top_text_2' ); ?></span>
                </div>
                <div class="footer-block">
                    <span class="footer-text"> <?php echo carbon_get_theme_option( 'crb_footer_bottom_text_1' ); ?>  </span>
                    <div class="break"></div>
                    <span class="footer-text"> <?php echo carbon_get_theme_option( 'crb_footer_bottom_text_2' ); ?></span>
                </div>
            </div>

            <a class="footer-content__center" href="<?php echo get_home_url() ;?>">
                <img src="<?php echo wp_get_attachment_image_src( carbon_get_theme_option('crb_footer_image'))[0]; ?>" alt="">
            </a>

            <div class="footer-content__right">
                <div class="footer-content__right__button-block">
                    <button class="button-round bc-dark-grey small" id="footer-scroll-up-button">
                        <i class="fas fa-chevron-up tc-lightest-grey"></i>
                    </button>
                </div>
                <div class="footer-content__right__text-block">
                    <span class="footer-text">Dizainas - Laura Diržytė</span>
                    <div class="break"></div>
                    <span class="footer-text">Svetainė - Dalius Slavickas</span>  
                </div>
            </div>

        </div>
    </footer><!-- #colophon -->

<?php wp_footer(); ?>
</body>
</html>
