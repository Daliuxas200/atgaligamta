<?php get_header(); ?>
<?php 
    $notfound_image = carbon_get_theme_option('crb_fourofour_image');
    $notfound_subtitle = carbon_get_theme_option('crb_fourofour_subtitle');
?>
    <header ></header>
    <div  class="outer-container notfound__outer">
        <div  class="inner-container">
            <div class="notfound">
                <div class="notfound__title">
                    <div class="notfound__four">4</div>
                    <div class="notfound__zero" style='background-image: url(" <?php echo wp_get_attachment_image_src($notfound_image, $size = 'medium')[0]; ?>'></div>
                    <div class="notfound__four">4</div>
                </div>
                <div class="notfound__subtitle"><?php echo $notfound_subtitle;?></div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>

