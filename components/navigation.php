<?php 
    $social = carbon_get_theme_option( 'crb_social' );
    $button_1 = carbon_get_theme_option( 'crb_menu_button_1' );
    $button_2 = carbon_get_theme_option( 'crb_menu_button_2' );
    if($button_1){
        $button_1_title = carbon_get_theme_option( 'crb_menu_button_1_title' );
    }
    if($button_2){
        $button_2_link = carbon_get_theme_option( 'crb_menu_button_2_link' );
        $button_2_title = carbon_get_theme_option( 'crb_menu_button_2_title' );
    }
?>

<nav class="main-nav">
    <div class="main-nav__mobile-menu" id="mobile-menu">
        <div class="main-nav__mobile-menu__container">
            <?php wp_nav_menu(); ?>

            <?php if($button_1): ?>
                <div id="popup-open-button-mobile" class='button-cta button-cta--1'><?php echo $button_1_title; ?></div>
            <?php endif; ?>

            <?php if($button_2): ?>
                <a href="<?php echo get_permalink($button_2_link[0]['id']); ?> ?>" class='button-cta button-cta--2'><?php echo $button_2_title; ?></a>
            <?php endif; ?>

            <div class="social-icons main-nav__mobile-menu__social-icons">
                <ul class="social-icons__list">
                    <?php if ( ! empty( $social ) && is_array( $social ) ) : ?>
                        <?php foreach ( $social as $value ):?>
                            <li>
                                <a href="<?php echo esc_url( $value['link'] ); ?>" target="_blank">
                                    <i class="fa <?php echo esc_attr( $value['icon_class'] ); ?>"></i>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <form class="main-nav__mobile-menu__search" id="menu_search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                <input type="text" class="main-nav__mobile-menu__search__input"  name="s" value="<?php echo get_search_query(); ?>">
                <button type="submit" class="main-nav__mobile-menu__search__button" form="menu_search" ><i class="fas fa-search"></i></button>
            </form>
        </div>
    </div>
    <div class="main-nav__search-bar" id="search-bar">
        <div class="main-nav__search-bar__container">
            <?php get_search_form(); ?>
        </div>
    </div>
    <div class="main-nav__outer-container">
        <div class="main-nav__container">
            <div class="main-nav__title"><a href="<?php echo get_home_url() ;?>"><?php echo get_bloginfo( 'name' ); ?></a></div>
            <div class="main-nav__menu">
                <?php wp_nav_menu(); ?>

                <?php if($button_1): ?>
                    <div id="popup-open-button" class='button-cta button-cta--1'><?php echo $button_1_title; ?></div>
                <?php endif; ?>

                <?php if($button_2): ?>
                    <a href="<?php echo get_permalink($button_2_link[0]['id']); ?> ?>" class='button-cta button-cta--2'><?php echo $button_2_title; ?></a>
                <?php endif; ?>
                
                <div class="social-icons main-nav__social-icons">
                    <ul class="social-icons__list">
                        <li id="search-bar-button" class="<?php echo esc_attr( $class ); ?>"><a href="" target="_blank"><i class="fas fa-search"></i></a></li>
                        <?php if ( ! empty( $social ) && is_array( $social ) ) : ?>
                            <?php foreach ( $social as $value ):?>
                                <li>
                                    <a href="<?php echo esc_url( $value['link'] ); ?>" target="_blank">
                                        <i class="fa <?php echo esc_attr( $value['icon_class'] ); ?>"></i>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div class="main-nav__button" id="mobile-menu-button">
                <div class="main-nav__button__middle-bar"></div>
            </div>
        </div>
    </div>
</nav>