<div class="popup" id="popup">
    <div class="popup__box">
        <div class="popup__close-button" id="popup-close-button"><i class="fas fa-times"></i></div>
        <div class="popup__title"><?php echo carbon_get_theme_option( 'crb_support_title' )?></div>
        <div class="popup__text"><?php echo carbon_get_theme_option( 'crb_support_text' )?></div>
        <div class="popup__buttons">
            <?php $supportOptions = carbon_get_theme_option( 'crb_support_options' ); ?>
            <?php foreach($supportOptions as $option):?>
                <a target="_blank" class="popup__button" href="<?php echo $option['link']; ?>"><img src="<?php echo wp_get_attachment_image_src($option['image'], $size = 'Medium')[0]; ?>" alt=""></a>
            <?php endforeach; ?>
        </div>
    </div>
</div>
