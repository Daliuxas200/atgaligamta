<?php 
    get_header(); 
    get_template_part( 'partials/header', 'common', 'Paieškos rezultatai');
?>
    <div class="outer-container archive">
        <div class="inner-container">
            <main id="main" class="page-content-full">
            <div class="content-search-bar">
                <?php global $wp_query;?>
                <?php if ( have_posts() ) : ?>
                    <div class="content-search-bar__title">Paieškos terminui "<?php echo get_search_query();?>" rasta rezultatų: <?php echo  $wp_query->found_posts; ?></div>
                <?php else : ?>
                    <div class="content-search-bar__title">Rezultatų terminui "<?php echo get_search_query();?>" nerasta, pabandykite patikslinti paiešką.</div>
                <?php endif; ?>
                    <?php get_search_form(); ?>
            </div>
            <?php 
                if ( have_posts() ) {
                    while ( have_posts() ) {
                        the_post();
                        get_template_part( 'partials/card',  'article-archive');
                    }
                    the_posts_pagination(array(
                        'mid_size'  => 2,
                        'prev_text' => __( '<button class="button-round small bc-dark-grey"><i class="fas fa-chevron-left tc-lightest-grey"></i></button>', 'textdomain' ),
                        'next_text' => __( '<button class="button-round small bc-dark-grey"><i class="fas fa-chevron-right tc-lightest-grey"></i></button>', 'textdomain' ),
                    ) );
                } else {
                    get_template_part( 'content', 'none' );
                }
                ?>
            </main>
        </div>
        <?php // get_template_part( 'partials/section',  'donate'); ?> 
    </div>
<?php get_footer(); ?>