<?php

/**
 * Carbon fields additions.
 */
require get_template_directory() . '/inc/carbon/carbon.php';

if ( ! function_exists( 'atgaligamta_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function atgaligamta_setup() {

		wp_enqueue_Scripts();
        wp_enqueue_style( 'default', get_stylesheet_uri() );
        wp_enqueue_style( 'main', get_template_directory_uri() . '/main.css' );
        wp_enqueue_script( 'script', get_template_directory_uri() . '/main.js');

		/*
		 * Make theme available for translation.
		 */
		load_theme_textdomain( 'atgaligamta', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );


		register_nav_menus(
			array(
				'primary-menu' => __( 'Primary', 'atgaligamta' )
			)
		);

        register_sidebar(
            array(
            'name'          => __( 'Main Sidebar', 'atgaligamta' ),
            'id'            => 'main-sidebar',
            'before_widget' => '<li class="widget">',
            'after_widget'  => "</li>\n",
            'before_title'  => '<h3 class="widget__title">',
            'after_title'   => "</h3>\n",
            )
        );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add support for Block Styles.
		// add_theme_support( 'wp-block-styles' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Add theme support for header image
        add_theme_support( 'custom-header' );

        // Custom search widget code
        function search_widget_form( $form ) {
            $form = '<form role="search" method="get" id="searchform" class="search-form" action="' . home_url( '/' ) . '" >
            <label class="screen-reader-text" for="widget-search">' . __( 'Ieškoti:' ) . '</label>
            <input class="search-form__input" type="text" value="' . get_search_query() . '" name="s" id="widget-search" />
            <input class="search-form__button" type="submit" id="searchsubmit" value="'. esc_attr__( 'Ieškoti' ) .'" />
            </form>';

            return $form;
        }
		add_filter( 'get_search_form', 'search_widget_form', 100 );
		
		// Custom excerpt length
		function my_excerpt_length($length){
			return 30;
		}
		add_filter('excerpt_length', 'my_excerpt_length');

		//  Excerpt ending
		function alx_excerpt_more( $more ) {
			return '&#46;&#46;&#46;';
		}
		add_filter( 'excerpt_more', 'alx_excerpt_more' );

		// Remove category, tag, name etc from name of archives
		function my_theme_archive_title( $title ) {
			if ( is_category() ) {
				$title = single_cat_title( '', false );
			} elseif ( is_tag() ) {
				$title = single_tag_title( '', false );
			} elseif ( is_author() ) {
				$title = '<span class="vcard">' . get_the_author() . '</span>';
			} elseif ( is_post_type_archive() ) {
				$title = post_type_archive_title( '', false );
			} elseif ( is_tax() ) {
				$title = single_term_title( '', false );
			}
		
			return $title;
		}
  
		add_filter( 'get_the_archive_title', 'my_theme_archive_title' );

		// Only search Posts
		// function SearchFilter($query) {
		// 	if ($query->is_search) {
		// 		$query->set('post_type', 'post');
		// 	}
		// 	return $query;
		// }
		// add_filter('pre_get_posts','SearchFilter');

		// Loading and booting Cargon Fields plugin
		require_once( 'vendor/autoload.php' );
   		\Carbon_Fields\Carbon_Fields::boot();

		add_image_size('chonky-medium',850,850,false);
	};
endif;

add_action( 'after_setup_theme', 'atgaligamta_setup' );