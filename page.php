<?php 
    get_header(); 
    get_template_part( 'partials/header', 'common', get_the_title());
?>
    <div class="outer-container">
        <div class="inner-container">
            <main id="main" class="page-content-full">
                <?php the_post(); ?>
                <?php the_content();?>
            </main>
        </div>
        <?php 
            $sections = carbon_get_the_post_meta( 'crb_page_sections');
            get_template_part( 'partials/sections', null, $sections  );
        ?>
    </div>
<?php get_footer(); ?>
