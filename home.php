<?php 
    get_header(); 
    get_template_part( 'partials/header', 'landing'  );
?>
    <div class="outer-container">
        <?php 
            $sections = carbon_get_theme_option( 'crb_sections' );
            get_template_part( 'partials/sections', null,  $sections  );
        ?>
    </div>
<?php get_footer(); ?>