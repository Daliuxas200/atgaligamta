<?php 
    get_header(); 
    $main_category = get_the_category($posts[0]->ID)[0];
    $main_category_link = get_term_link($main_category->term_id);
?>

<header id="header" class="article-header">
    <div class="article-header__container">
        <h1 class="article-header__title">
            <?php echo get_the_title(); ?>
        </h1>
        <div class="article-header__subtitle">
            <div class="article-header__date"><?php echo get_the_date(); ?></div>
            <a class="article-header__breadcrumb" href="<?php echo $main_category_link;?>"><?php echo $main_category->name; ?></a>
        </div>
    </div>
</header>

<div class="outer-container">
    <div class="inner-container">
        <main id="main" class="page-content">
            
            <div class="post__featured" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')"></div>
            <?php the_post(); ?>
            <?php the_content(); ?>
        </main>
        <?php if ( is_active_sidebar( 'main-sidebar') ) : ?>
        <aside id="sidebar" class="page-sidebar">
            <ul>
                <?php dynamic_sidebar( 'Main sidebar' ); ?>
            </ul>
        </aside>
        <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>

