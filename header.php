<!doctype html>
<html lang="lt-LT">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="Atgal į Gamtą, tai ne pelno siekianti organizacija kurios tikslas padėti sužeistiems laukiniams gyvūnams grįžti į gamtą."> 
    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/regular.min.css" integrity="sha384-rbtXN6sVGIr49U/9DEVUaY55JgfUhjDiDo3EC0wYxfjknaJamv0+cF3XvyaovFbC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/brands.min.css" integrity="sha384-yZSrIKdp94pouX5Mo4J2MnZUIAuHlZhe3H2c4FRXeUcHvI2k0zNtPrBu8p3bjun5" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.0/css/fontawesome.min.css" integrity="sha384-syoT0d9IcMjfxtHzbJUlNIuL19vD9XQAdOzftC+llPALVSZdxUpVXE0niLOiw/mn" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/2cb445a4a5.js" crossorigin="anonymous"></script>
    
    <!-- Google analytics     -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-LP6M36MXXE"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-LP6M36MXXE');
    </script>

    <?php wp_head(); ?>
</head>

<body <?php body_class('website-container'); ?>>

<?php include 'components/navigation.php';?>

<?php carbon_get_theme_option( 'crb_menu_button_1' ) && include 'components/popup.php'; ?>

