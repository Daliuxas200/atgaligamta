<?php 
    /* Template Name: Page only with Sections ( for correct padding ) */ 
    get_header(); 
    get_template_part( 'partials/header', 'common', get_the_title());
?>
    <div class="outer-container">
        <?php 
            $sections = carbon_get_the_post_meta( 'crb_page_sections');
            get_template_part( 'partials/sections', null, $sections  );
        ?>
    </div>
<?php get_footer(); ?>
